=================================
Install raspbian on Raspi 3 B +
=================================

First download and write image to minicard. You can refer to (https://www.raspberrypi.org/documentation/installation/installing-images/) 

Follow the offcial Raspberry Pi tutorial will give you a raspbian without desktop evioronment.

LXDE desktop and applications:

.. code:: html

    #Desktop (LXDE)
    
    $ sudo apt-get install --no-install-recommends xserver-xorg
    $ sudo apt-get install --no-install-recommends xinit
    $ sudo apt-get install lxde-core lxappearance
    $ sudo  apt-get install lightdm
    
    #sudo no password
    
    $ sudo passwd  # set root password
    $ su root  # login as root
    $ visudo
    #add this line to the end
    pi ALL=(ALL) NOPASSWD:ALL
    #disable root login
    $ sudo passwd -l root
    
    #Auto login desktop
    
    #Edit /etc/lightdm/lightdm.conf
    #Set autologin-user=pi
    $ sudo reboot
    
    #Applications
    #Firefox web browser
    $ sudo apt-get install firefox
    
    $ sudo apt install curl unzip git
    
    #geit text editor
    $ sudo apt install gedit