====================================
Install arch linux in a USB thumb
====================================

The settups and installation can follow the tutorials:

http://www.valleycat.org/linux/arch-usb.html?i=1 

#### To change mirrorlist

You may need to change mirrorlist to reflect your location, arch linux provide a tool to generate a mirrorlist base  your country you locate

// a website to generete your country mirrorlist 

https://www.archlinux.org/mirrorlist/

// Change name to backup your oringinal mirrorlist file

$ sudo mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak

Change your contents in /etc/pacman.d/mirrorlist with generated mirrorlist, comment out at least one mirror on the mirrorlist.

.. code:: html

    #Update the morrorlist
    
    $ sudo pacman -Syy
    
    #Install LXDE desktop
    
    $ sudo -S lxde