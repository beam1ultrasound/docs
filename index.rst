.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Tactic Link Documentations
---------------------------

ERP Getstart
"""""""""""""""


.. toctree::
   :caption: ERP Getstart
   
   d/manual/inventory

Use Data
"""""""""""""



.. toctree::
   :caption: Data
   
   d/data/analytics



Blogs
""""""""

關於數字化，數據，競爭力的啟發與思考

.. toctree::

   blog/index
